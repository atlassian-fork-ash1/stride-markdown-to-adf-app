/**
 * This is our main module for this example.
 * In this module we load in a markdown file from the filesystem.
 * Convert it into ADF using the MD to ADF Service.
 * And send it forward towards the conversation where our bot was mentioned.
 */

var express = require('express');
var router = express.Router();
const fs = require('fs');
const path = require('path');
var messages = require('../helper/messages');
const axios = require("axios");
const API_BASE_URL = 'https://api.atlassian.com';

/**
 * Function to send a message in Markdown.
 * This function will read markdown from a file (./md-example-files/md-exampl-1.md to be exact).
 * This markdown well then be converted into Atlassian Document Format (ADF) using the convertMarkdownToDoc function.
 * We then send the resulting message back to the conversation where we were mentioned.
 */

router.sendMarkdownMessage = async function sendMarkdownMessage(cloudId, conversationId) {
    var markdown = fs.readFileSync('./md-example-files/md-example-1.md','utf8');
    
    const document = await this.convertMarkdownToDoc(markdown);
    console.log("Markdown converted: " + JSON.stringify(document));
    messages.sendMessage(cloudId, conversationId, document, function (err, response) {
      if (err)
        console.log(err);
    });
  }
  
  /**
   * This function converts markdown to the Atlassian Document Format using 
   * the converter service provided by Atlassian.
   * This service is available at https://api.atlassian.com/pf-editor-service/convert?from=markdown&to=adf
   */
  
  router.convertMarkdownToDoc = async function convertMarkdownToDoc(markdown) {
  const response = await axios.request({
    url: API_BASE_URL + '/pf-editor-service/convert?from=markdown&to=adf',
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    data: {input: markdown},
  });
  return(response.data);
  }

  module.exports = router;