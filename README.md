# README #

## What is this? ##

This is an example app for Atlassian Stride.
This app shows how you can read markdown, convert it to Atlassian Document Format and post it in a Stride conversation through the REST API.

## How do I get set up? ##

### Prerequisites ###

* Make sure you have [node 8.x](https://nodejs.org/en/) or above on your machine
* Make sure you have the latest version of [ngrok](https://ngrok.com/) installed
* Sign up for an account on [https://developer.atlassian.com](https://developer.atlassian.com)
* [Create a new app](https://developer.atlassian.com/apps/create). Let's call it the 'MD-to-ADF Example App'. 
* Make sure to enable the Stride API for your app during the creation process.
* Once your app is created you need to:
  * Go into the 'Enabled APIs' tab and copy the Stride API Client Id and Client Secret, we'll need this later.

### Running the app ###

* Clone this repository to your local machine.
* Open a terminal window and go into your local repository to run ```npm install```.
* Once that command is finished you'll need to run ```PORT=3333 CLIENT_ID={clientId} CLIENT_SECRET={clientSecret} node app.js```
* Now open a second terminal window and run ```ngrok http 3333```.
* Copy the https url from your Ngrok output, this is where your app is now available to the internet.
* Go back to your app page on [https://developer.atlassian.com/apps](https://developer.atlassian.com/apps)
* Go into the 'Install' tab and paste your ngrok url + '/descriptor' (<your-ngrok-url>/descriptor) in the Descriptor url field.
* Click 'Refresh'
* You app is now live and ready to be installed.

### Installing the app ###

* Copy the installation url from your 'Install' tab in your app page.
* Go into a Stride room and click on the 'Apps' glance (The little nut icon) on the right side of your screen, this will open up a sidebar.
* Click on the + icon at the top of the sidebar, this will open internal marketplace and show the installed apps for your Stride room.
* Click on the 'Add custom app' link at the top of the page, this will open up a dialog where you can paste your installation URL. This will load your app's information into the dialog.
* Click 'Agree' to install the app.

### Seeing it in action ###

* Mention the app in your room (you can look up the @mention name in your app management screen or by looking at the apps sidebar) and it will send a message with the converted markdown.
* Feel free to change the markdown in the md-example-1.md file in the md-example-files directory in your local repository. Any change in this file will be visible as soon as you mention your app again.
* You can also create a different source for markdown and have that converted and posted into a room.

### How the code works ###
This example focusses on converting markdown from a file into Atlassian Document Format.
The bulk of our example code can be found in **./routes/markdown.js.**

When you mention the app in a conversation, this will trigger the **/bot-mention** endpoint (describe in our app descriptor).
This bot-mention endpoint gets captures in our **./app.js** file and calls our markdown module to send a message using the **sendMarkdownMessage** function.
The **sendMarkdownMessage** function will read in our example markdown file from the filesystem and call the **convertMarkdownToDoc** function (available in our markdown module), we are using await/async here because we need to wait on the results of this function before we can proceed.

This function will use the Atlassian provided MD-to-ADF service to convert the markdown code into ADF and will return this ADF format.
Our **sendMarkdownMessage** will now call the **./helper/messages.js** module to send a message with the ADF content to our conversation.

### Need help ###

Need help with this sample code or want to ask a question about Stride app development?  Head over to the [Atlassian Developer Community](https://community.developer.atlassian.com/) and create a new topic in the [Stride Development Category](https://community.developer.atlassian.com/c/stride-development).