const PORT = process.env.PORT;
const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const API_BASE_URL = 'https://api.atlassian.com';

if (!PORT || !CLIENT_ID || !CLIENT_SECRET) {
  console.log("Usage:");
  console.log("PORT=<http port> CLIENT_ID=<app client ID> CLIENT_SECRET=<app client secret> node app.js");
  process.exit();
}

const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const app = express();

/**
 * Let's set up some routes and helper modules.
 */
var messages = require('./helper/messages');
var index = require('./routes/index');
var markdown = require('./routes/markdown');

app.use('/', index);
app.use(bodyParser.json());
app.use(express.static('.'));


/**
 * Let's send a Markdown message when somebody mentions us.
 * Checkout the ./routes/markdown.js module to see how this code works.
 */
app.post('/bot-mention',
  function (req, res) {
    markdown.sendMarkdownMessage(req.body.cloudId, req.body.conversation.id);
    res.sendStatus(204);
  }
);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

 console.log("error found: " + err);
})
  

http.createServer(app).listen(PORT, function () {
  console.log('App running on port ' + PORT);
});
